package com.orange.dahmakan.di.builder

import com.orange.dahmakan.ui.root.RootModule
import com.orange.dahmakan.ui.root.RootScreen
import com.orange.dahmakan.ui.splash.SpashModule
import com.orange.dahmakan.ui.splash.SplashScreen
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule{
    @ContributesAndroidInjector(modules = [SpashModule::class])
    internal abstract fun contributeSplashActivity(): SplashScreen

    @ContributesAndroidInjector(modules = [RootModule::class])
    internal abstract fun contributeRootActivity(): RootScreen
}
