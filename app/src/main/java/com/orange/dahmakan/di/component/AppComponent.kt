package com.orange.dahmakan.di.component

import android.app.Application
import com.orange.dahmakan.DahmakanApp
import com.orange.dahmakan.di.builder.ActivityBuildersModule
import com.orange.dahmakan.di.module.AppModule
import javax.inject.Singleton
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, ActivityBuildersModule::class])
interface AppComponent : AndroidInjector<DahmakanApp> {

    override fun inject(app: DahmakanApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

}
