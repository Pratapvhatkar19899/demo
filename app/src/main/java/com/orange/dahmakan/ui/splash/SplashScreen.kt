package com.orange.dahmakan.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.orange.airasia.ui.splash.SpashNavigator
import com.orange.dahmakan.BR
import com.orange.dahmakan.R
import com.orange.dahmakan.appbase.BaseActivity
import com.orange.dahmakan.databinding.ActivitySplashScreenBinding
import com.orange.dahmakan.ui.root.RootScreen
import com.orange.dahmakan.ui.splash.SplashViewModel
import retrofit2.Retrofit
import javax.inject.Inject

class SplashScreen : BaseActivity<ActivitySplashScreenBinding, SplashViewModel>(), SpashNavigator{

    @Inject
    internal lateinit var mSpalshViewModel: SplashViewModel


    override val bindingVariable: Int
        get() = BR.viewModel

    override val viewModel: SplashViewModel
        get() = mSpalshViewModel

    override val layoutId: Int
        get() = R.layout.activity_splash_screen

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()

        Handler().postDelayed({
            var intent = Intent(applicationContext,RootScreen::class.java)
            startActivity(intent)
        }, 5000)

    }
}
