package com.orange.dahmakan.ui.splash

import androidx.lifecycle.ViewModelProvider
import com.orange.dahmakan.appbase.ViewModelProviderFactory
import dagger.Module
import dagger.Provides

@Module
class SpashModule {
    @Provides
    internal fun SpalshViewModelProvider(splashViewModel: SplashViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(splashViewModel)
    }

    @Provides
    internal fun provideSplashViewModel(): SplashViewModel {
        return SplashViewModel()
    }
}
