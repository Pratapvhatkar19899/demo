package com.orange.dahmakan.ui.root

import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.*
import com.orange.dahmakan.BR
import com.orange.dahmakan.R
import com.orange.dahmakan.appbase.BaseActivity
import com.orange.dahmakan.databinding.ActivityRootScreenBinding
import com.orange.dahmakan.ui.root.adapter.FeedsAdapter
import com.orange.dahmakan.ui.root.models.Feed
import com.valdesekamdem.library.mdtoast.MDToast
import kotlinx.android.synthetic.main.activity_root_screen.*
import javax.inject.Inject


class RootScreen : BaseActivity<ActivityRootScreenBinding, RootViewModel>(), RootNavigator {

    @Inject
    internal lateinit var mViewModel: RootViewModel

    var activityRootScreenBinding: ActivityRootScreenBinding? = null

    var mLayoutManager: LinearLayoutManager = LinearLayoutManager(this);

    var mAdapter: FeedsAdapter = FeedsAdapter()

    override val bindingVariable: Int
        get() = BR.viewModel

    override val viewModel: RootViewModel
        get() = mViewModel

    override val layoutId: Int
        get() = R.layout.activity_root_screen


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        centerToolbarText("")
        activityRootScreenBinding = super.viewDataBinding
        mViewModel.setmNavigator(this)
        mViewModel.getFeedsData()
        loader.visibility = View.VISIBLE
    }

    private fun initUI() {
        mLayoutManager.setOrientation(RecyclerView.VERTICAL)
        recyclerView.setLayoutManager(mLayoutManager)
        recyclerView.setItemAnimator(DefaultItemAnimator())
//        recyclerView.setAdapter(mAdapter)
//        mAdapter.notifyDataSetChanged()
//        indicator.attachToRecyclerView(recyclerView);
//        val snapHelper: SnapHelper = LinearSnapHelper()
//        snapHelper.attachToRecyclerView(recyclerView)
    }

    private fun centerToolbarText(title: String) {
        val mTitleTextView = AppCompatTextView(this)
        mTitleTextView.text = title
        mTitleTextView.setSingleLine()
        mTitleTextView.textSize = 25f
        val layoutParams = ActionBar.LayoutParams(
            ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.WRAP_CONTENT
        )
        layoutParams.gravity = Gravity.CENTER
        supportActionBar?.setCustomView(mTitleTextView, layoutParams)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
    }

    override fun onRecivedFeeds(feed: Feed) {
        loader.visibility = View.INVISIBLE
        centerToolbarText(feed.title)
        initUI()
        mAdapter.setCustomContext(applicationContext)
        mAdapter.setFeedList(feed)
        recyclerView.setAdapter(mAdapter)
        mAdapter.notifyDataSetChanged()

    }

    override fun onError(error: String) {
        MDToast.makeText(
            applicationContext,
            "Network Error",
            MDToast.LENGTH_LONG,
            MDToast.TYPE_ERROR
        ).show();
    }


}
