package com.orange.dahmakan.ui.root

import com.orange.dahmakan.appbase.BaseViewModel
import com.orange.dahmakan.ui.root.models.Feed
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import retrofit2.http.GET


class RootViewModel : BaseViewModel<RootNavigator>() {

    interface OrdersApi {
        @GET("/s/2iodh4vg0eortkl/facts.json")
        open fun getFeedData(): Flowable<Feed>?
    }

    fun getFeedsData() {

        retrofit
            .create(OrdersApi::class.java)
            .getFeedData()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnError(Consumer {
                print("Error")
            })
            ?.subscribe(Consumer
            {
                getmNavigator()?.onRecivedFeeds(it)
            }, Consumer { getmNavigator()!!.onError(it.toString()) })
            .let {
                it?.let { it1 ->
                    compositeDisposable?.add(
                        it1
                    )
                }
            }
    }

}
