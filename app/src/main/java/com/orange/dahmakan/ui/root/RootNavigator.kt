package com.orange.dahmakan.ui.root

import com.orange.dahmakan.ui.root.models.Feed

interface RootNavigator {
    fun onRecivedFeeds(feed: Feed)
    fun onError(error: String)
}