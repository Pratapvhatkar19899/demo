package com.orange.dahmakan.ui.root.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.orange.dahmakan.R
import com.orange.dahmakan.appbase.BaseViewHolder
import com.orange.dahmakan.databinding.ItemEmptyBinding
import com.orange.dahmakan.databinding.ItemOrdersBinding
import com.orange.dahmakan.ui.root.models.Feed
import java.text.SimpleDateFormat
import java.util.*

class FeedsAdapter : RecyclerView.Adapter<BaseViewHolder>() {

    var feedlist: Feed? = null
    var customContextVar: Context? = null

    override fun getItemViewType(position: Int): Int {
        if (feedlist == null)
            return VIEW_TYPE_EMPTY

        if (feedlist!!.rows.size == 0)
            return VIEW_TYPE_EMPTY

        return VIEW_TYPE_NORMAL
    }


    fun setFeedList(feed: Feed) {
        this.feedlist = feed;
    }

    fun setCustomContext(context: Context){
        this.customContextVar  = context;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> {
                val mBinding: ItemOrdersBinding = ItemOrdersBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
                NotificationViewHolder(mBinding)
            }

            VIEW_TYPE_EMPTY -> {
                val mBinding: ItemEmptyBinding = ItemEmptyBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
                EmptyViewHolder(mBinding)
            }
            else -> {
                val mBinding: ItemOrdersBinding = ItemOrdersBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
                NotificationViewHolder(mBinding)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount(): Int {
        if (feedlist != null) {
            return feedlist?.rows?.size!!
        } else {
            return 0
        }
    }

    inner class NotificationViewHolder(binding: ItemOrdersBinding) :
        BaseViewHolder(binding.card) {
        var bi = binding
        override fun onBind(position: Int) {
            feedlist.let {
                bi.titleText.setText(it?.rows?.get(position)?.title)
                bi.infoText.setText(it?.rows?.get(position)?.description)

                customContextVar?.let { it1 ->
                    Glide.with(it1)
                        .load(it?.rows?.get(position)?.imageHref)
                        .placeholder(R.drawable.ina)
                        .into(bi.imageView)
                };
            }
        }
    }

    inner class EmptyViewHolder(binding: ItemEmptyBinding) :
        BaseViewHolder(binding.card) {
        var bi = binding
        override fun onBind(position: Int) {

        }
    }

    private fun getDateTime(s: String): String? {
        try {
            val sdf = SimpleDateFormat("hh:mm aa")
            val netDate = Date(s.toLong() * 1000)
            return sdf.format(netDate)
        } catch (e: Exception) {
            return e.toString()
        }
    }

    companion object {
        const val VIEW_TYPE_NORMAL = 1
        const val VIEW_TYPE_EMPTY = 2

    }
}
