package com.orange.dahmakan.ui.splash

import com.orange.airasia.ui.splash.SpashNavigator
import com.orange.dahmakan.appbase.BaseViewModel

class SplashViewModel: BaseViewModel<SpashNavigator>()
