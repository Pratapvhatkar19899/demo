package com.orange.dahmakan.ui.root

import androidx.lifecycle.ViewModelProvider
import com.orange.dahmakan.appbase.ViewModelProviderFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Inject

@Module
class RootModule{


    @Provides
    internal fun ViewModelProvider(rootViewModel: RootViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(rootViewModel)
    }

    @Provides
    internal fun provideRootViewModel(): RootViewModel {
        return RootViewModel()
    }


//    @Provides
//    fun provideLinearLayoutManager(rootScreen: RootScreen): LinearLayoutManager? {
//        return LinearLayoutManager(rootScreen)
//    }
//
//    @Provides
//    fun provideOrderAdapter(): OrdersAdapter? {
//        return OrdersAdapter()
//    }
}
